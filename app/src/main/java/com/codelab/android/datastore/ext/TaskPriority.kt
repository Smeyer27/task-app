package com.codelab.android.datastore.ext

enum class TaskPriority {
    HIGH, MEDIUM, LOW
}