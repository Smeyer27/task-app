package com.codelab.android.datastore.model

import com.codelab.android.datastore.ext.SortOrder

data class TasksUiModel(
    val tasks: List<Task>,
    val showCompleted: Boolean,
    val sortOrder: SortOrder
)